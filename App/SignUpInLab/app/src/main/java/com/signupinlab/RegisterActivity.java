package com.signupinlab;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethod;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterActivity extends AppCompatActivity {
    DBHandler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        handler = new DBHandler(this);
        SQLiteDatabase database = handler.getWritableDatabase();
        handler.onCreate(database);
    }

    public void registerButton(View view){
        EditText emailEntry = findViewById(R.id.app_register_edittext_email);
        EditText passEntry = findViewById(R.id.app_register_edittext_password);
        String email = String.valueOf(emailEntry.getText());
        String pass = String.valueOf(passEntry.getText());
        //ToDo: Check if either edit text is empty!
        if (handler.userExists(email, null)){
            TextView errorText = findViewById(R.id.app_register_textview_error);
            errorText.setText(getResources().getString(R.string.string_error_textview_userexists));
            errorText.setVisibility(View.VISIBLE);
        }
        else{
            handler.createUser(email, pass);
            backButton(view);
        }
    }

    public void backButton(View view){
        InputMethodManager imm = (InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        finish();
    }
}