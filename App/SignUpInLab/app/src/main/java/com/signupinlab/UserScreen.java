package com.signupinlab;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.TextView;

public class UserScreen extends AppCompatActivity {
    DBHandler handler;
    String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_screen);
        handler = new DBHandler(this);
        SQLiteDatabase database = handler.getWritableDatabase();
        handler.onCreate(database);
        Intent emailIntent = getIntent();
        email = emailIntent.getStringExtra("email");
        TextView emailText = findViewById(R.id.app_user_textview_userEmail);
        emailText.setText(email);
    }

    public void logoutButton(View view){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        sp.edit().clear().commit();
        Intent mainSwitch = new Intent(this, MainActivity.class);
        startActivity(mainSwitch);
        finish();
    }

    @Override
    protected void onStop(){
        super.onStop();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor edit = sp.edit();
        edit.putString("email", email);
        edit.apply();
    }
}