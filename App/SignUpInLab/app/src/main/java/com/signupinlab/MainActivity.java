package com.signupinlab;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    DBHandler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        handler = new DBHandler(this);
        SQLiteDatabase database = handler.getWritableDatabase();
        handler.onCreate(database);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        String email = sp.getString("email", "na");
        Log.d("User already logged in", email);
        if (handler.userExists(email, null)){
            Intent userScreenSwitch = new Intent(this, UserScreen.class);
            userScreenSwitch.putExtra("email", email);
            userScreenSwitch.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(userScreenSwitch);
            finish();
        }
    }

    public void registerButton(View view){
        Intent registerSwitch = new Intent(this, RegisterActivity.class);
        startActivity(registerSwitch);
    }

    public void signInButton(View view){
        EditText emailEntry = findViewById(R.id.app_main_edittext_email);
        EditText passEntry = findViewById(R.id.app_main_edittext_password);
        String email = String.valueOf(emailEntry.getText());
        String pass = String.valueOf(passEntry.getText());
        ProgressBar progressBar = findViewById(R.id.app_main_progress_loading);
        progressBar.setVisibility(View.VISIBLE);
        //ToDo: Check if either edit text is empty!
        if (handler.userExists(email, pass)){
            Intent userScreenSwitch = new Intent(this, UserScreen.class);
            userScreenSwitch.putExtra("email", email);
            userScreenSwitch.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(userScreenSwitch);
            finish();
        }
        else{
            registerButton(view);
        }
    }
}