package com.signupinlab;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHandler extends SQLiteOpenHelper {
    static SQLiteDatabase db;
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "userdata.db";
    private static final String TABLE_USERS = "table_users";
    private static final String COL_USERID = "user_id";
    private static final String COL_USER_EMAIL = "user_email";
    private static final String COL_USER_PASS = "user_pass";
    private static final String CREATE_TABLE_USERS = "CREATE TABLE IF NOT EXISTS table_users" +
            "(user_id integer PRIMARY KEY NOT NULL, " +
            "user_email text NOT NULL," +
            "user_pass text NOT NULL);";

    public DBHandler(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        try{
            db.execSQL(CREATE_TABLE_USERS);
        }
        catch (Exception error){
            System.out.println("Database already exists");
        }
        this.db = db;
    }

    public boolean userExists(String email, String password){
        String fetchUser;
        String currentCursor = "na";
        if (password != null){
            fetchUser = "SELECT user_email, user_pass FROM " + TABLE_USERS;
        }
        else{
            fetchUser = "SELECT user_email FROM " + TABLE_USERS;
        }
        Cursor cursor = db.rawQuery(fetchUser, null);
        if (cursor.moveToFirst()){
            do {
                currentCursor = cursor.getString(0);
                if (currentCursor.equals(email)) {
                    if (password == null) {
                        return true;
                    } else {
                        currentCursor = cursor.getString(1);
                        break;
                    }
                }
            }
            while (cursor.moveToNext());
        }
        if (currentCursor.equals(password)){
            return true;
        }
        else{
            return false;
        }
    }

    public boolean createUser(String email, String password){
        ContentValues values = new ContentValues();
        values.put(COL_USER_EMAIL, email);
        values.put(COL_USER_PASS, password);
        try{
            db.insert(TABLE_USERS, null, values);
            Log.d("User creation SUCCESS", values.toString());
            return true;
        }
        catch (Exception error){
            Log.d("User creation FAILED", values.toString());
            return false;
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
        this.onCreate(db);
    }
}
